26 Total Questions - Need to parse this down to 25.

[[
Which three (3) are advantages of Continuous Integration? (Choose three)
]]
1. Improved readability of code
*2. Defects are detected and fixed quickly
*3. Reduction in repetitive manual processes
*4. Immediately know how a check-in affected the build
5.	Reduction for the need for multiple check-ins of code

[[
Identify a benefit of establishing naming standards for code?
]]
*1. To make the code more readable
2.	To ensure code compiles quickly
3.	To assist IDEs with code auto-complete features
4.	To identify the name of the developer that worked on the code
5.	To make it easy to distinguish between different software products

[[
What is code coverage?
]]
1.	An insurance policy that covers the code for defects
*2. The degree to which the system has been exercised by tests
3.	The number of developers that understand how the code works
4.	The percentage of code in version control included in a build
5.	A warranty period that covers code defects for a predetermined amount of time post-release

[[
If a Continuous Integration build fails, who should ensure the build is repaired?
]]
*1. The person who broke the build
2.	The tester responsible for validating builds
3.	The next person who needs the build to complete successfully
4.	The person assigned to the configuration management role within the team

[[
When does the Development Team participate in Product Backlog refinement?
]]
*1. Anytime during the Sprint
2.	Only during the Sprint Planning meeting
3.	Only during refinement meetings planned by the Product Owner
4.	Never. It is the sole responsibility of the Product Owner to refine the Product Backlog

[[
Which two (2) statements define Technical Debt? (Choose two)
]]
1.	Code that has not been commented or documented
2.	The money an organization owes to tool and hardware vendors
3.	The average time or money a Scrum Team spends per Sprint on bug fixes
*4. A term representing the eventual consequences of poor technical choices
*5. Degraded application performance resulting from delayed infrastructure maintenance

[[
Identify three (3) attributes of a good bug report? (Choose three)
]]
1.	Multiple, related bugs in one report
*2. Detailed steps to reproduce the issue
3.	A title with the highest-level component affected
*4. Build/version or environment where bug was found
5.	A description of what the tester thinks should have happened
*6. Supporting documentation, including screenshots when user interfaces are involved

[[
Which two (2) statements are true about canceling a Sprint? (Choose two)
]]
1.	You can not cancel a Sprint
*2. Only the Product Owner can cancel a Sprint
3.  Cancellation of Sprints is a regular occurrence
*4. You cancel Sprints if the Sprint Goal becomes obsolete
5.  You cancel Sprints if they will not be finished in time

[[
What are two (2) reasons to automate the software build process? (Choose two)
]]
*1. To get feedback on changes to the code early and often
2.	Code reviews are much faster if you automate your build
3.	An automated build process is required to run an automated test
*4. Automation improves the quality of software by making builds less error-prone

[[
What is pair programming?
]]
1.	Code reviews limited to two programmers
2.	A developer and a tester work together to write and test code
3.	The Scrum Team is divided into several two-person development teams
*4. Two developers writing code together, providing constant peer review

[[
What does a test written with Test Driven Development represent?
]]
1.	A bug that will be uncovered
2.	An assignment from the lead quality engineer
*3. A technical requirement that must be satisfied
4.	Something that completes the test coverage of a system

[[
Which of the following describes an architecture spike?
]]
1.	A fundamental technical problem found in an existing application
2.	The result of an architectural planning effort during a sprint planning meeting
3.	A decision made by a systems architect to settle disagreement within a Development Team
*4. A small development or research activity to learn about technical elements of a proposed solution

[[
What is the value of refactoring code?
]]
1.	To keep the code moving
2.	To eliminate code defects
*3. To improve code design without changing functionality
4.	To make the code faster at runtime
5.	To have all code in a single file for improved readability

[[
What are two (2) shortcomings of code coverage as a measurement for how well a system or product is tested? (Choose two)
]]
1.	It is too complicated to explain to management
*2. Code coverage may not provide functional coverage
3.	Code coverage metrics vary by development platform (e.g., .NET, Java)
*4. Code coverage does not ensure that the most important or highest risk areas of the code are being exercised by tests

[[
Which is the best answer for how often the build should be executed?
]]
1.	Once per day
*2. Whenever new or changed code is checked into version control
3.	As often as possible, and certainly prior to the end of the Sprint
4.	Whenever the QA group becomes uncertain that the system works

[[
Should User Stories be part of the documentation generated by a Scrum project?
]]
1.	Never
2.	Always
3.	If the architect requires it
*4. If they are part of the "Definition of Done"
5.	They must be provided to the Development Team as part of the user specifications documents

[[
Identify three (3) characteristics of a high-quality unit test. (Choose three)
]]
*1. Each test is independent of other unit tests
2.	Reduces the need for code usage documentation
3.	They exercise the persistence layer of a solution
*4. Each test makes assertions about only one logical concept
*5. Code in each test is as small as possible while maintaining readability of the code

[[
What would be used to give a skeletal outline of a screen representing its basic structure and functionality?
]]
1.	Mockup
2.	Sitemap
3.	Modeling
*4. Wireframe
5.	User Story

[[
If there were five (5) teams working on a major release for a product how many product backlogs should there be?
]]
*1. 1
2.	2
3.	5
4.	It depends

[[
Which three (3) roles are prescribed for a Scrum team?  (Choose three)
]]
1.  Solution Architect
*2. Scrum Master
*3. Product Owner
4.  Project Manager
*5. Developer/Tester

[[
Which statement is true about the daily stand-up meeting?
]]
1.	The entire scrum team gets together to discuss the status
2.	It is a brief daily meeting for the stakeholders of the project
*3. They are <em>not</em> used to provide status updates to the project manager
4.	It should take as long as needed so everyone gets a chance to update the scrum master

[[
What is the purpose of a spike?
]]
1.	Adding extra stories to an existing iteration
2.	A significant increase in velocity from one iteration to another
3.	Temporarily adding additional resources to a scrum team to complete a sprint
*4. An activity in an iteration intended to reduce uncertainty in a feature, technology, or process

[[
At the end of a Sprint the Development Team should have what?
]]
1.	The final product
2.	A plan for the next Sprint
*3. A working version of the product
4.	An approximate layout for the product
5.	Whatever they happen to finish by the end of the Sprint

[[
Who is accountable in a development team?
]]
1.	The Scrum Team
2.	The Scrum Master
3.	The Solution Lead
4.	The Product Owner
*5. The Development Team

[[
Which of these is NOT true about Planning Poker?
]]
1.	If all estimators selected the same value that becomes the estimate
2.	All cards are selected individually in private and revealed simultaneously
3.	If not all estimators selected the same value they discuss their estimates
*4. If the selected estimators do not agree then the story is returned to the product backlog
5.	The numbers on the cards represent a unit of time agreed upon by each group of estimators

[[
Suppose you are a developer, and a Manager of another team informs you that
there is a bug in your backlog that needs to be fixed immediately due to a road block.

What is a proper course of action?
]]
1.	Fix the bug immediately
*2. Address the Product Owner
3.	Edit the backlog and promote the bug to a higher priority
4.	Wait until the next backlog grooming to address the problem
