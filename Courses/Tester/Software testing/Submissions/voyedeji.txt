(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Victor Oyedeji
(Course Site): Wikipedia
(Course Name): Software testing
(Course URL): https://en.wikipedia.org/wiki/Software_testing
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Which is NOT a Software Testing role?
(A): Test lead
(B): Tester
(C): Manager
(D): Test Analyst
(E): Developer
(Correct): E
(Points): 1
(CF): A developer is technically not a software testing role.
(WF): A developer is technically not a software testing role.
(STARTIGNORE)
(Hint):
(Subject): Software Testing Roles
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): truefalse
(Category): 18
(Grade style): 0
(Random answers): 0
(Question): Grey-Box testing deals with both the Source code and the User Interface.
(A): True
(B): False
(Correct): A 
(Points): 1
(CF): Grey-Box testing deals with both the Source code and the User Interface.
(WF): Grey-Box testing deals with both the Source code and the User Interface.
(STARTIGNORE)
(Hint):
(Subject): Software Testing
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)



(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): What type of testing seeks to verify the interfaces between components against a software design?
(A): Acceptance
(B): System
(C): Integration
(D): Black Box
(E): White Box
(Correct): C
(Points): 1
(CF): Integration testing seeks to verify the interfaces between components against a software design.
(WF): Integration testing seeks to verify the interfaces between components against a software design.
(STARTIGNORE)
(Hint):
(Subject): Software Testing
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): This type of testing usually is done at the Tester/Developers site.
(A): Alpha
(B): Beta
(Correct): A
(Points): 1
(CF): Alpha testing is usually done as the Tester/Developers site
(WF): Alpha testing is usually done as the Tester/Developers site
(STARTIGNORE)
(Hint):
(Subject): Software Testing Types
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)



(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): What is the main difference between an Agile approach and the Waterfall approach?
(A): Agile approach is a sequential development approach, where Waterfall is a collaborative approach between self-organizing teams.
(B): Waterfall uses a rapid construction of prototypes, instead of large amounts of up-front planning.
(C): Waterfall approach is a sequential development approach, where Agile is a collaborative approach between self-organizing teams.
(D): Agile and Waterfall approach are actually the same.
(Correct): C
(Points): 1
(CF): Waterfall approach is a sequential development approach, where Agile is a collaborative approach between self-organizing teams.
(WF): Waterfall approach is a sequential development approach, where Agile is a collaborative approach between self-organizing teams.
(STARTIGNORE)
(Hint):
(Subject): Software Testing Types
(Difficulty): Intermediate
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Usually when does a Software Tester actually begin testing?
(A): After the software has been completely developed
(B): When the Stakeholders give final approval
(C): Before the requirements are created
(D): Whenever the Tester feels necessary to begin
(E): As soon as executable software (even partially) exists
(Correct): E
(Points): 1
(CF): As soon as executable software (even partially) exists, testing can begin.
(WF): As soon as executable software (even partially) exists, testing can begin.
(STARTIGNORE)
(Hint):
(Subject): Software Testing 
(Difficulty): Intermediate
(Applicability): course
(ENDIGNORE)



(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Which is NOT a Non-Functional requirement of software quality?
(A): Scalability
(B): Testablilty
(C): Maintainability
(D): Technicalablity
(E): Security
(Correct): D
(Points): 1
(CF): Technicalablity is a functional requirement of software quality.
(WF): Technicalablity is a functional requirement of software quality.
(STARTIGNORE)
(Hint):
(Subject): Software Testing 
(Difficulty): Intermediate
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): In its simplest form, a successful test is one that _______.
(A): has 100% coverage
(B): is executed on time
(C): finds a bug
(D): has a collaborative effort
(E): doesn't find a bug
(Correct): C
(Points): 1
(CF): According to Glenford J. Myers in 1979, a successful test is one that finds a bug.
(WF): According to Glenford J. Myers in 1979, a successful test is one that finds a bug.
(STARTIGNORE)
(Hint):
(Subject): Software Testing 
(Difficulty): Advanced
(Applicability): course
(ENDIGNORE)


(Type): truefalse
(Category): 18
(Grade style): 0
(Random answers): 0
(Question): It is common to run a Smoke test at the end of the testing process.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Smoke tests are commonly run at the Beginning of the testing process, to ensure the main functionalities of the software is intact before going deeper into the process.
(WF): Smoke tests are commonly run at the Beginning of the testing process, to ensure the main functionalities of the software is intact before going deeper into the process
(STARTIGNORE)
(Hint):
(Subject): Software Testing Types
(Difficulty): Intermediate
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Automated testing is most useful when running what types of tests?
(A): Unit testing 
(B): Regression testing
(C): Sanity testing
(D): Performance testing
(E): Security testing
(Correct): B
(Points): 1
(CF): Automated testing is most useful during Regression testing, as it does require a well-developed test suite of testing scripts in order to be truly useful.
(WF): Automated testing is most useful during Regression testing, as it does require a well-developed test suite of testing scripts in order to be truly useful.
(STARTIGNORE)
(Hint):
(Subject): Software Testing 
(Difficulty): Advanced
(Applicability): course
(ENDIGNORE)
