(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
2 Axure
6 Communication
0 Generic
8 HTML
1 Java
3 JIRA
4 OOP
5 SDLC
7 Unix Scripting

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Harmon Keener
(Course Site):Udemy
(Course Name): Consulting Skills Series Communication
(Course URL):https://www.udemy.com/consulting-skills-series-communication/?dtcode=NXFwWSz1vMzW
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): When speaking during a meeting you are hosting, you should address __________
(A): everyone, as it is appropriate.
(B): the highest ranking person in the room, because they have the most influence.
(C): your closest superior, because you report immediately to them.
(D): the person you are most familiar with, because it will make you more comfortable.
(Correct): A
(Points): 1
(CF): 
(WF): Since meetings should include only the people relevant to them, each person in the meeting is a valid candidate to be addressed, and likely will NEED to be addressed because they will have important input to help reach your meeting�s objectives. 
(STARTIGNORE)
(Hint):
(Subject):Effective Meetings
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): Why is making and distributing an agenda for your meeting important?
(A): It provides a clear focus of intent and communicates what is expected to be covered. 
(B): It provides evidence to your superiors that you are taking the meeting seriously.
(C): It gives you an anchor to return to in case you lose your train of thought while speaking.
(D): It gives you an excuse to reign in people that can't stay on the topics you want to discuss.
(Correct): A
(Points): 1
(CF): 
(WF): Agendas provide a roadmap, so that everyone can know what to expect in the meetings they will attend. This will better help them prepare and contribute to the meeting.
(STARTIGNORE)
(Hint):
(Subject): Effective Meetings
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): Why is it important to follow up with attendees after you hold a meeting?
(A): You can ensure that any tasks they need to perform are clearly explained and assigned and you can reinforce attendees' buy-in.
(B): It will keep the meeting topic on their minds.
(C): Most of them were probably off daydreaming or working on something else, so you want to try to maximize the potential for them to understand what your meeting was about.
(D): It is important to have a large email presence, to get your name out there.
(Correct): A
(Points): 1
(CF): 
(WF): Following up with meeting attendees gives you and the other attendees a referenceable record of who is responsible for what task and helps to ensure that their buy-in is maintained. 
(STARTIGNORE)
(Hint):
(Subject): Effective Meetings
(Difficulty): Advanced
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): Which is not a consideration to make before scheduling a conference call?
(A): Make the group as large as you can, so that all stakeholders are sure to have representation in the call
(B): Pick a time that is appropriate for all callers
(C): Determine the call is actually needed, so you are not tying up people in an unnecessary conference call.
(D):How to properly use the phone and conference call software. 
(Correct): A
(Points): 1
(CF): 
(WF): When having a conference call, smaller groups are preferred, to help keep the meeting focused and under control.
(STARTIGNORE)
(Hint):
(Subject): Conducting Effective Conference Calls
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What is one way you can keep people on your conference call focused and attentive?
(A): Ask for frequent feedback and don't speak for too long at once 
(B): Use Powerpoint slides, so people can follow along.
(C): Read aloud the provided handouts during the call.
(D): Run your call as long as possible to make sure everyone understands.
(Correct): A
(Points): 1
(CF): 
(WF): Speaking too long and not soliciting feedback bores the attendees; People have relatively short attention spans and just listening to a caller drone on will lose their attention.
(STARTIGNORE)
(Hint):
(Subject): Conducting Effective Conference Calls
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What is not considered a benefit to taking notes with pen and paper during a conference call?
(A): Written notes can be locked in a desk and are more secure than electronic notes stored on a vulnerable electronic device.
(B): The reduced noise from using pen and paper vs a keyboard will make it easier to focus on the meeting topics.
(C): Taking notes during the call will help you follow along. 
(D): Transcribing the notes electronically afterward will improve information retention.
(Correct): A 
(Points): 1
(CF): 
(WF): The security of written notes as compared to the security of typed notes is not a concern (beyond the concern that all discussed topics should be considered confidential, regardless of the method used to record them) 
(STARTIGNORE)
(Hint):
(Subject): Conducting Effective Conference Calls
(Difficulty): Advanced
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): On what activities will you likely spend the 45% of your week that is taken up by dealing with email?
(A): It is estimated that you will only spend approximately 28% of your work week handling email. 
(B): Formal/Informal status messages.
(C): Requesting information/clarification.
(D): Supply documentation and report deliverables.
(Correct): A
(Points): 1
(CF): 
(WF): A little over a quarter of a consultant's 40 hour work week is spent reading, writing, and replying to emails. At least it's not almost a half!
(STARTIGNORE)
(Hint):
(Subject): The Consultant's Guide To Email
(Difficulty): Beginner
(Applicability): 
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): When is it ok to send an angry email?
(A): Never 
(B): When a correspondent has misrepresented you, making you look bad to the rest of the email recipients.
(C): When a correspondent is refusing to cooperate.
(D): When a correspondent has been willfully uninvolved with the project for the entire duration and now is requesting that you catch them up and pick up their slack.
(Correct): A
(Points): 1
(CF): 
(WF): You should never send an angry email. Emails are a digital representation of you, and you should always be professional. 
(STARTIGNORE)
(Hint):
(Subject): The Consultant's Guide To Email
(Difficulty): Intermediate
(Applicability): 
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What is not something to be concerned about when sending email? 
(A): That your email is written in a format, font, and style that makes it stand out and be noticed. 
(B): That your email can be held as evidence indefinitely, in some states.
(C): That your email should be professional, succinct, and factual.
(D): That you are soliciting feedback from your recipients as appropriate.
(Correct): A
(Points): 1
(CF): 
(WF): Even though email is a digital representation of yourself, it should be professional and mindful of the possibility of litigation. Also, email is about communication, and feedback is a necessary component of communication.
(STARTIGNORE)
(Hint):
(Subject):The Consultant's Guide To Email
(Difficulty): Advanced
(Applicability): 
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): According to some surveys, what do almost all presentation attendees admit to doing during presentations?
(A): Daydream.
(B): Sleep.
(C): Talk on their cell phones.
(D): Pay their undivided attention.
(Correct): A
(Points): 1
(CF): 
(WF): 91% admit to daydreaming. 39% admit to sleeping. According to the survey, at best, 9% might sit in rapt attention for the duration of the presentation.
(STARTIGNORE)
(Hint):
(Subject):Creating Effective Presentations
(Difficulty): Beginner 
(Applicability): 
(ENDIGNORE)

(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): Which of the following statements is not a good guideline for presentation slides?
(A): 2 or 3 points can fit on a single slide if the points are small.
(B): Fonts and colors should be carefully considered and kept as simple as possible to improve readability.
(C): Try to order slides so that topics and points flow together smoothly.
(D): Keep text to a minimum because people read quickly and have short attention spans.
(Correct): A
(Points): 1
(CF): 
(WF): It is best to keep slides limited to 1 point per slide. Additional points on a slide can cause people to lose focus on the item you are discussing if they read ahead to the next points or their attention span runs out because there was too much text.
(STARTIGNORE)
(Hint):
(Subject):Creating Effective Presentations
(Difficulty): Intermediate
(Applicability): 
(ENDIGNORE)

(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What is not a good way to connect with your audience?
(A): Always act confident, putting on a facade if you have to, because it's important to seem knowledgeable, even if you are unfamiliar with the topic.
(B): Tell them stories that help bridge the gap between your topic and a common or relatable experience, because they will identify with you.
(C): Look at your audience, even if you can't actually make eye contact, because it will give a more personal feel to the presentation.
(D): Smile, genuinely, and relax.
(Correct): A
(Points): 1
(CF): 
(WF): The audience wants you to do well, and admitting your lack of confidence, familiarity or comfort will disarm them. 
(STARTIGNORE)
(Hint):
(Subject):Creating Effective Presentations
(Difficulty): Advanced
(Applicability): 
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What is not a component of the Triune Brain Theory which governs our reactions to people?
(A): Encephalopodic Brain.
(B): Lizard Brain
(C): Mammal Brain.
(D): Human Brain.
(Correct): A 
(Points): 1
(CF): 
(WF): The Lizard brain, Mammalian brain, and Human brain make up the Triune Brain Theory.
(STARTIGNORE)
(Hint):
(Subject): The Consultant's Guide to People
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What is true about executives when they are speaking with you?
(A): Time is their most precious asset.
(B): They appreciate a long, detailed explanation of a topic so that they get the best possible understanding.
(C): They like a lead-up from the summary to the big "bottom line" punch, in that order.
(D): They like to fix things, so bring them problems which you could use guidance toward a solution.
(Correct): A 
(Points): 1
(CF): 
(WF): Executives are very busy, and will appreciate your understanding and preparation if you give them a quick "elevator pitch" rather than eating up time they could be using to do other things.
(STARTIGNORE)
(Hint):
(Subject):The Consultant's Guide to People
(Difficulty): Intermediate
(Applicability): 
(ENDIGNORE)

(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): When dealing with adverse people __________
(A): listen to them carefully, repeat their concerns back, and answer them directly.
(B): answer directly, tell the truth, and try to bundle up multiple issues from the same person into a collective package to save time.
(C): it's ok to lose your temper if they are already being hostile.
(D): be friendly, keep your temper, but take the opportunity to punish them for causing problems.
(Correct): A
(Points): 1
(CF): 
(WF): It is important to treat even your adversaries with respect and try to resolve disputes professionally and peacefully. You want to keep them from interfering with your and, by extension, the company's success, and one of the best ways to do that is to get them on your side.
(STARTIGNORE)
(Hint):
(Subject):The Consultant's Guide to People
(Difficulty): Advanced 
(Applicability): 
(ENDIGNORE)



