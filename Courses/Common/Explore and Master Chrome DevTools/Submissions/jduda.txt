(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Joseph Duda
(Course Site): discover-devtools.codeschool.com
(Course Name): Explore and Master Chrome DevTools
(Course URL): http://discover-devtools.codeschool.com/
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): What is "pretty print mode" useful for?
(A): Printing nicely colored pages.
(B): Presenting webpages using the user's default fonts set by their operating system.
(C): Limiting the number of images on a webpage to save network resources.
(D): Taking a snapshot of the current system state.
(E): Making condensed code more readable.
(Correct): E
(Points): 1
(CF): Pretty print mode is a feature that makes it much easier to read minimized files.
(WF): Pretty print mode is a feature that makes it much easier to read minimized files.
(STARTIGNORE)
(Hint):
(Subject): Chrome DevTools
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): Which of the following are appropriate ways to minimize the size of your JavaScript files?
(A): Eliminate whitespace.
(B): Combine multiple JavaScript files into a single file.
(C): Only use lowercase letters when writing the code.
(D): Shorten variable names.
(E): Write all the JavaScript using jQuery.
(Correct): A, B, D
(Points): 1
(CF): Anything that will create fewer and smaller files without losing integrity should be appropriate.
(WF): Anything that will create fewer and smaller files without losing integrity should be appropriate.
(STARTIGNORE)
(Hint):
(Subject): Chrome DevTools
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): You can disable the cache in Chrome by using an incognito window.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): In addition to using an incognito window, you can disable the cache in the DevTools extension.
(WF): In addition to using an incognito window, you can disable the cache in the DevTools extension.
(STARTIGNORE)
(Hint):
(Subject): Chrome DevTools
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): Heap snapshots are useful for determining the cause of a memory leak.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): You can compare two heap snapshots to see what is causing the difference (i.e. the memory leak).
(WF): You can compare two heap snapshots to see what is causing the difference (i.e. the memory leak).
(STARTIGNORE)
(Hint):
(Subject): Chrome DevTools
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): It is not possible to toggle an element's :hover state without actually hovering over it.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): You can toggle the :active, :hover, :focus, and :visited states by using checkboxes in the styles tab.
(WF): You can toggle the :active, :hover, :focus, and :visited states by using checkboxes in the styles tab.
(STARTIGNORE)
(Hint):
(Subject): Chrome DevTools
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): Which of the following should be done to optimize a website?
(A): Remove any unneeded network requests.
(B): Minimize your files as much as possible.
(C): Serve small images at the appropriate size so the browser does not need to scale them down.
(D): Have the CSS load before the JavaScript in the head of an HTML file.
(E): All of the above.
(Correct): E
(Points): 1
(CF): All of the above will help with optimization.
(WF): All of the above will help with optimization.
(STARTIGNORE)
(Hint):
(Subject): Chrome DevTools
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)