(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Megan Barton
(Course Site): Lynda
(Course Name): Agile Project Management Foundations
(Course URL): https://www.lynda.com/Business-Project-Management-tutorials/Agile-Project-Management/122428-2.html
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 15
(Grade style): 1
(Random answers): 1
(Question): Which three phases of the agile lifecycle are looped through for each iteration/sprint until the project is complete?
(A): Envision
(B): Speculate
(C): Explore
(D): Adapt
(E): Close
(Correct): B,C,D
(Points): 1
(CF): Envision happens once at the beginning of a project and Close happens once at the end.
(WF): Envision happens once at the beginning of a project and Close happens once at the end.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): Which option is something that is NOT typically part of requirements gathering?
(A): A business analyst works ahead of the development team
(B): New features are defined
(C): Existing features that are no longer needed are identified
(D): A demo of the project is given to stakeholders
(Correct): D
(Points): 1
(CF): Giving demos to stakeholders is not done during requirements gathering
(WF): Giving demos to stakeholders is not done during requirements gathering
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 0
(Question): True or false: In agile project management, changes in business priority cannot be accommodated.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Accommodating changes and being flexible is a key part of agile project management.
(WF): Accommodating changes and being flexible is a key part of agile project management.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)
