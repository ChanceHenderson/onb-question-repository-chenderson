(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Stephen Cerbone
(Course Site): Lynda.com
(Course Name):Agile Project Management
(Course URL): http://www.lynda.com/Business-Project-Management-tutorials/Agile-Project-Management/122428-2.html
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): What stages are part of the agile project life cycle?
(A): Speculate Stage
(B): Explore Stage
(C): Develop Stage
(D): Collaboration Stage
(E): Adapt Stage
(Correct): A, B, E
(Points): 1
(CF): Agile project stages are: envision, speculate, explore, adapt, and close.
(WF): Agile project stages are: envision, speculate, explore, adapt, and close.
(STARTIGNORE)
(Hint):
(Subject): Agile Project Life cycle
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): The close stage is the stage when deliverables are completed and captured lessons are learned.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): In the close stage deliverables are completed and captured lesson are learned.
(WF): In the close stage deliverables are completed and captured lesson are learned.
(STARTIGNORE)
(Hint):
(Subject): Agile Methodology
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which of the following is true about the explore phase?
(A): Development team works on the highest priority items first.
(B): Scrum master assigns tasks to each team member.
(C): Explore phase doesn't end until all team members have finished their work, regardless of time estimates.
(Correct): A
(Points): 1
(CF): Development team works on the highest priority items first.
(WF): Scrum master doesn't assign tasks to each member and explore phase ends when features are completed or when the sprint time is up.
(STARTIGNORE)
(Hint):
(Subject): Agile Methodology
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Sprint structure is the length of a sprint and the team members involved with the project.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Sprint structure is the length of a sprint and the number of features planned for a sprint.
(WF): Sprint structure is the length of a sprint and the number of features planned for a sprint.
(STARTIGNORE)
(Hint):
(Subject): Agile Project Management
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): It is best for a new agile team to work on the hardest items of their backlog first.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): It is best for a new agile team to work on the easier features first.
(WF): It is best for a new agile team to work on the easier features first.
(STARTIGNORE)
(Hint):
(Subject): Agile Project Management
(Difficulty): Intermidiate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): It is important in stand-up meetings to
(A): be standing up.
(B): limit meeting to necessary members.
(C): resolve issues before leaving.
(D): not hold to agile methods/values strongly.
(E): have a loose and flexible meeting time.
(Correct): A, B
(Points): 1
(CF): It is important in stand-up meetings to be standing up, limit the meeting to necessary members and to keep the time consistent.
(WF): It is important in stand-up meetings to be standing up, limit the meeting to necessary members and to keep the time consistent.
(STARTIGNORE)
(Hint):
(Subject): Agile Project Management
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): What are important things to observe in stand-up meetings?
(A): Is the team collaborating?
(B): Are there common problems?
(C): Are risks surfacing?
(D): Is the list of issues growing?
(E): 
(Correct): A, B, C, D
(Points): 1
(CF): All of these things are important to observe in stand-up meetings.
(WF): All of these things are important to observe in stand-up meetings.
(STARTIGNORE)
(Hint):
(Subject): Agile Project Management
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): A burn-down chart is the number of features compared to the estimated time they will take to complete.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): A burn down chart shows the work left to complete a project.
(WF): A burn down chart shows the work left to complete a project.
(STARTIGNORE)
(Hint):
(Subject): Agile Project Management
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): The PDCA cycle helps teams
(A): collaborate between members.
(B): increase how much they recycle.
(C): communicate with the project owner.
(D): evaluate their project goals.
(E): increase efficiency with non co-located employees.
(Correct): A
(Points): 1
(CF): The PDCA cycle helps teams collaborate between members.
(WF): The PDCA cycle helps teams collaborate between members.
(STARTIGNORE)
(Hint):
(Subject): Agile Project Management
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Developing lessons learned should be a constant process, not only addressed at the end of a sprint.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Developing lessons learned should be a constant process.
(WF): Developing lessons learned should be a constant process.
(STARTIGNORE)
(Hint):
(Subject): Agile Project Management
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)