(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
2 Axure
6 Communication
0 Generic
8 HTML
1 Java
3 JIRA
4 OOP
5 SDLC
7 Unix Scripting

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): wthorpe
(Course Site): N/A
(Course Name): Scrum Guide
(Course URL): http://www.scrumguides.org/docs/scrumguide/v1/Scrum-Guide-US.pdf#zoom=100
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): How long has scrum been around as a framework?
(A): Since the 1960s
(B): Since the 1990s
(C): Since 2009
(D): Since the 1920s
(Correct): B
(Points): 1
(CF): Scrum is relatively new as far as methodologies go, but it has been around 20 years now.
(WF): Scrum is relatively new as far as methodologies go, but it has been around 20 years now.
(STARTIGNORE)
(Hint):
(Subject): Agile Scrum
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): What are some of the primary Scrum specific events?
(A): Sprint Planning
(B): Daily Scrum
(C): Daily Retrospective
(D): Sprint Backlog Review
(Correct): A,B
(Points): 1
(CF): You only do a retrospective at the end of a sprint, and you do not review sprint backlog in any specific event.
(WF): You only do a retrospective at the end of a sprint, and you do not review sprint backlog in any specific event.
(STARTIGNORE)
(Hint):
(Subject): Agile Scrum
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): The product owner has control of which items? (mark all that apply)
(A): Priority order of items in product backlog
(B): Content and details of product backlog items
(C): The number of backlog items to be completed in a sprint
(D): The cancellation of a sprint due to obsolete goals
(Correct): A,B,D
(Points): 1
(CF): Only the development team controls how many items they can complete in a sprint, otherwise the product owner controls most backlog functions.
(WF): Only the development team controls how many items they can complete in a sprint, otherwise the product owner controls most backlog functions.
(STARTIGNORE)
(Hint):
(Subject): Agile Scrum
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): The Scrum master approves delivery time estimates for backlog items.
(A): True 
(B): False
(Correct): B
(Points): 1
(CF): The scrum master does not approve anything, he facilitates and helps.
(WF): The scrum master does not approve anything, he facilitates and helps.
(STARTIGNORE)
(Hint):
(Subject): Agile Scrum
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): The Scrum master helps find techniques for effective product backlog management.
(A): True 
(B): False
(Correct): A
(Points): 1
(CF): The scrum master facilitates all parts of the process, including finding effective techiques for the product owner's use.
(WF): The scrum master facilitates all parts of the process, including finding effective techiques for the product owner's use.
(STARTIGNORE)
(Hint):
(Subject): Agile Scrum
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Sprint cancellation is rare, what might cause such a cancellation?
(A): Re-estimated scope of backlog items is too large to fit in sprint
(B): The sprint goal is no longer relevant to the company's direction
(C): A team member leaves the company and a replacement cannot be found
(D): The scrum master feels that the team will not make the sprint goal
(Correct): B
(Points): 1
(CF): Failing to meet the goals of a sprint will not cancel the sprint, including being down by a member.  Only the goal becoming obsolete can cancel a sprint.
(WF): Failing to meet the goals of a sprint will not cancel the sprint, including being down by a member.  Only the goal becoming obsolete can cancel a sprint.
(STARTIGNORE)
(Hint):
(Subject): Agile Scrum
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): For Scrum, if you are using a one month sprint, how long should the normal Sprint planning session last?
(A): 8 hours
(B): 4 hours
(C): 16 hours
(D): 3 hours
(Correct): A
(Points): 1
(CF): A sprint planning for a one month sprint is fairly standard at 8 hours for Scrum.
(WF): A sprint planning for a one month sprint is fairly standard at 8 hours for Scrum.
(STARTIGNORE)
(Hint):
(Subject): Agile Scrum
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which of the following is not important to keep the same in the daily scrum?
(A): Location
(B): Time
(C): Length
(D): Atendees
(E): Order of presentation
(Correct): E
(Points): 1
(CF): It is actually helpful to vary the order to keep people more attentive. The rest help reduce complexity.
(WF): It is actually helpful to vary the order to keep people more attentive. The rest help reduce complexity.
(STARTIGNORE)
(Hint):
(Subject): Agile Scrum
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): What are some of the activities that occur during the sprint review? (mark all that apply)
(A): Dev team members discuss what went well during the sprint
(B): Dev team members discuss what they will do during the next day
(C): Dev team members determine how the work of the next sprint will be achieved
(D): Dev team members demonstrate what work was finished and answer questions about it
(Correct): A,D
(Points): 1
(CF): The sprint review is not a daily scrum, so the next days work is not significant.  It is also not the place to start planning the next sprint.
(WF): The sprint review is not a daily scrum, so the next days work is not significant.  It is also not the place to start planning the next sprint.
(STARTIGNORE)
(Hint):
(Subject): Agile Scrum
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): What are some examples of progress monitoring tools for Scrum?
(A): Burn-down chart
(B): Burn-up Chart
(C): Product backlog
(D): Product Owner monitor
(Correct): A,B,C
(Points): 1
(CF): The product owner maintains the backlog and participates in meetings, but is not involved in the day to day scrum activities.
(WF): The product owner maintains the backlog and participates in meetings, but is not involved in the day to day scrum activities.
(STARTIGNORE)
(Hint):
(Subject): Agile Scrum
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


