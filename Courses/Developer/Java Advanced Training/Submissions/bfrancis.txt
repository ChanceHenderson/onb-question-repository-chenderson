(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Brad Francis
(Course Site): www.lynda.com
(Course Name): Java Advanced Training
(Course URL) : http://www.lynda.com/Java-tutorials/Java-Advanced-Training/107061-2.html
(Discipline): Technical 
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): What interface does a class that is going to be used in a tree set need to implement?
(A): Object
(B): Serializable
(C): Comparable
(D): IsSerializable
(E): None of the above
(Correct): C
(Points): 1
(CF): One must implement the comparable interface to define how the tree set will order.
(WF): One must implement the comparable interface to define how the tree set will order.
(STARTIGNORE)
(Hint):
(Subject): Java Advanced Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1 
(Grade style): 0
(Random answers): 1
(Question): The following is an example of what new feature of java 1.7?
    try (BufferedReader br =
                   new BufferedReader(new FileReader(path))) {
        return br.readLine();
    }
(A): try with resources
(B): Buffering
(C): try catch block
(D): this is not a new feature of java 1.7
(Correct): A
(Points): 1
(CF): This is an example of java 1.7's try with resource functionality.
(WF): This is an example of java 1.7's try with resource functionality.
(STARTIGNORE)
(Hint):
(Subject): Java Advanced Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): In java 1.7 what can a switch statement do that it was not able to previously?
(A): Enums are not allowed to be used in java 1.7
(B): Nothing is new for a switch statement for java 1.7
(C): A string can now be used in a switch statement
(D): None of the above
(Correct): C
(Points): 1
(CF): Strings are now able to be used as part of the switch statement in java 1.7
(WF): Strings are now able to be used as part of the switch statement in java 1.7
(STARTIGNORE)
(Hint):
(Subject): Java Advanced Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): true/false
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Hashsets are much faster at retrieval than treesets? 
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Hashsets are faster because they do not have to worry about ordering and do not have to evaluate the data before returning.
(WF): Hashsets are faster because they do not have to worry about ordering and do not have to evaluate the data before returning.
(STARTIGNORE)
(Hint):
(Subject): Java Advanced Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1 
(Grade style): 0
(Random answers): 1
(Question): What is the purpose of putting this.count += value; in a sychronized block like below?
    synchronized(this){
       this.count += value;   
    }
(A): makes sure multiple threads can enter at the same time to modify the count
(B): ensures that only 1 thread at a time can enter the synchronized block of code at a time
(C): increases the chance of deadlock
(D): it does nothing really
(Correct): B
(Points): 1
(CF): The synchronized block prevents deadlock and will ensure only 1 thread is allowed in at a time.
(WF): The synchronized block prevents deadlock and will ensure only 1 thread is allowed in at a time.
(STARTIGNORE)
(Hint):
(Subject): Java Advanced Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)
