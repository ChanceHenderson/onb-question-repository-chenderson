(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Joseph DiBrango
(Course Site): www.lynda.com
(Course Name): Java Essentials Training
(Course URL) : http://www.lynda.com/Java-tutorials/Essential-Training/86005-2.html
(Discipline): Technical 
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): What is NOT a principle of Java?
(A): Robust and secure
(B): It is not achitecture neutral and not portable
(C): Interpreted, threaded and dynamic
(D): Simple and object oriented
(E): High performance
(Correct): B
(Points): 1
(CF): Java is architecture neutral and portable.
(WF): Java is architecture neutral and portable.
(STARTIGNORE)
(Hint):
(Subject): Java Essentials Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 1 
(Grade style): 0
(Random answers): 1
(Question): A String is a complex type?
(A): True
(B): False
(Correct): A
(Points): 1
(CF): String is an object and therefore is a complex type.
(WF): String is an object and therefore is a complex type.
(STARTIGNORE)
(Hint):
(Subject): Java Essentials Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Passing by reference in Java is?
(A): When a variable is passed to a function, a copy is made 
(B): When you do not pass any variables at all
(C): When a variable is passed to a function, the code in the method operates on the original value
(D): None of the above
(Correct): C
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Java Essentials Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 1
(Random answers): 1
(Question): Which of the following are primitive types?
(A): Date & Number 
(B): Boolean & String 
(C): Integer, Double, Float, Boolean 
(D): int, double, float
(Correct): d
(Points): 1
(CF): int, double, float are primitive types.
(WF): int, double, float are primitive types.
(STARTIGNORE)
(Hint):
(Subject): Java Essentials Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Which of the following operation(s) is/are required to convert java source code into output form?
(A): Compilation
(B): Interpretation
(C): Both A and B
(D): None of the above
(Correct): C
(Points): 1
(CF): Java is compiled and interpreted.
(WF): Java is compiled and interpreted.
(STARTIGNORE)
(Hint):
(Subject): Java Essentials Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Java code is interpreted first then compiled?
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Java code is compiled first, then interpreted.
(WF): Java code is compiled first, then interpreted.
(STARTIGNORE)
(Hint):
(Subject): Java Essentials Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Java programs have what extension?
(A): .java
(B): .txt
(C): .jav
(D): .class
(E): None of the above
(Correct): D
(Points): 1
(CF): Java program files have the .class extension.
(WF): Java program files have the .class extension.
(STARTIGNORE)
(Hint):
(Subject): Java Essentials Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Declarations must appear at the start of the body of a Java method?
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Declarations can appear anywhere within the body of the method.
(WF): Declarations can appear anywhere within the body of the method.
(STARTIGNORE)
(Hint):
(Subject): Java Essentials Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 1
(Random answers): 1
(Question): The concept of multiple inheritance is implemented by? 
(A): Extending two or more classes
(B): Extending one class and implementing one or more interfaces
(C): Implementing two or more interfaces
(D): All of the above
(Correct): B,C
(Points): 1
(CF): Multiple inheritence is accomplished by extending a class and implementing one or more interfaces.
(WF): Multiple inheritence is accomplished by extending a class and implementing one or more interfaces.
(STARTIGNORE)
(Hint):
(Subject): Java Essentials Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)